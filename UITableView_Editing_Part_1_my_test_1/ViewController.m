//
//  ViewController.m
//  UITableView_Editing_Part_1_my_test_1
//
//  Created by Vladyslav Bedro on 6/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ViewController.h"
#import "VBStudent.h"
#import "VBGroup.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

// Properties
@property (weak, nonatomic) IBOutlet UITableView* mainTableView;
@property (strong, nonatomic) NSMutableArray* groupsArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@end


#pragma mark - Life circle -

@implementation ViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.mainTableView.editing = YES;
    [self uploadInitialData];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDataSource -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return [self.groupsArray count];
}

- (nullable NSString*) tableView: (UITableView*) tableView titleForHeaderInSection: (NSInteger) section
{
    NSString* nameGroup = [[self.groupsArray objectAtIndex: section] name];
    return nameGroup;
}

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    VBGroup* group = [self.groupsArray objectAtIndex: section];
    NSInteger numberOfStudents = [group.students count];
    
    return numberOfStudents;
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    static NSString* identifier = @"Cell_ID";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1
                                                        reuseIdentifier: identifier];
    }
    
    VBGroup* group = [self.groupsArray objectAtIndex: indexPath.section];
    VBStudent* student = [group.students objectAtIndex: indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@", student.firstName, student.lastName];
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%1.2f", student.averageGrade];
    
    if(student.averageGrade > 4.0)
    {
        cell.detailTextLabel.textColor = [UIColor greenColor];
    }
    else if(student.averageGrade > 3.0)
    {
        cell.detailTextLabel.textColor = [UIColor orangeColor];
    }
    else if(student.averageGrade > 2.0)
    {
        cell.detailTextLabel.textColor = [UIColor redColor];
    }
    
    return cell;
}

- (BOOL) tableView: (UITableView*) tableView canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}

- (void)      tableView: (UITableView*) tableView
moveRowAtIndexPath: (NSIndexPath*) sourceIndexPath
                toIndexPath: (NSIndexPath*) destinationIndexPath
{
    VBGroup* sourceGroup       = [self.groupsArray objectAtIndex: sourceIndexPath.section];
    VBStudent* sourceStudent = [sourceGroup.students objectAtIndex: sourceIndexPath.row];
    
    NSMutableArray* tempArray = [NSMutableArray arrayWithArray: sourceGroup.students];
    
    if(sourceIndexPath.section == destinationIndexPath.section)
    {
        [tempArray exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
        sourceGroup.students = tempArray;
    }
    else
    {
        [tempArray removeObject:sourceStudent];
        sourceGroup.students = tempArray;
        
        VBGroup* destinationGroup       = [self.groupsArray objectAtIndex: destinationIndexPath.section];
        tempArray = [NSMutableArray arrayWithArray: destinationGroup.students];
        [tempArray insertObject: sourceStudent atIndex: destinationIndexPath.row];
        destinationGroup.students = tempArray;
    }
    
}


#pragma mark - UITableViewDelegate -

- (UITableViewCellEditingStyle) tableView: (UITableView*) tableView editingStyleForRowAtIndexPath: (NSIndexPath*) indexPath
{
    return UITableViewCellEditingStyleNone;
}


#pragma mark - Private methods -

- (void) uploadInitialData
{
    self.groupsArray = [NSMutableArray array];
    
    for (int i = 0; i < ((arc4random() % 6) + 5); i++)
    {
        VBGroup* group = [[VBGroup alloc] init];
        group.name = [NSString stringWithFormat:@"Group №%d", i];
        
        NSMutableArray* muttableArray = [NSMutableArray array];
        
        for(int j = 0; j < ((arc4random() % 11) + 15); j++)
        {
            VBStudent* student = [VBStudent randomStudent];
            [muttableArray addObject: student];
        }
        
        group.students = muttableArray;
        
        [self.groupsArray addObject: group];
    }
    
    [self.mainTableView reloadData];
}


#pragma mark - Actions -

- (IBAction) onEditButtonPressed: (UIBarButtonItem*) sender
{
    BOOL isEditing = self.mainTableView.editing;

    [self.mainTableView setEditing: !(isEditing)
                                       animated: NO];

    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;

    if (self.mainTableView.editing)
    {
        //self.editButton.;
        item = UIBarButtonSystemItemDone;
    }

    self.editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                                                 target:self
                                                                                                                 action:@selector(onEditButtonPressed:)];
    
}


@end










































