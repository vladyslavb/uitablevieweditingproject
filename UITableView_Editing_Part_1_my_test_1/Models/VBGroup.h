//
//  VBGroup.h
//  UITableView_Editing_Part_1_my_test_1
//
//  Created by Vladyslav Bedro on 6/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VBGroup : NSObject

// Properties
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSArray*  students;

@end
