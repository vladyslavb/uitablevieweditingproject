//
//  VBStudent.h
//  UITableView_Editing_Part_1_my_test_1
//
//  Created by Vladyslav Bedro on 6/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VBStudent : NSObject

// Properties
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (assign, nonatomic) CGFloat    averageGrade;

// Methods
+ (VBStudent*) randomStudent;

@end
